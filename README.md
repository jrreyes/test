# README

For this test, we have 2 apps, one is the backend in Rails (API using GraphQL) and a frontend in React.


Please follow the next instructions for run both apps:

* Backend

Install dependencies:  
-> bundle install

Creating Database and dummy data:  
-> rails db:create db:migrate db:seed

Run backend:  
-> rails s -p 3000

For testing w/rspec please follow this:  
-> bundle exec rspec

Endpoint:  
-> http://localhost:3000/graphql

If you want check the endpoint for example in insomnia you can use this graphql query:  
    { random_word { id name }}
    
	
	
--------------------------------------------------------

* Frontend

Install dependencies:  
-> yarn install

For run:  
-> yarn run dev-server




