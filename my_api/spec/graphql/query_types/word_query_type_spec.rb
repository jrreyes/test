RSpec.describe QueryTypes::WordQueryType do
  # avail type definer in our tests
  types = GraphQL::Define::TypeDefiner.instance
  # create fake word using the word factory
  let!(:words) { create_list(:word, 3) }

  describe 'querying all words' do

    it 'has a :words that returns a Word type' do
      expect(subject).to have_field(:words).that_returns(types[Types::WordType])
    end

    it 'returns all our created words' do
      query_result = subject.fields['words'].resolve(nil, nil, nil)

      # ensure that each of our words is returned
      words.each do |word|
        expect(query_result.to_a).to include(word)
      end

      # we can also check that the number of lists returned is the one we created.
      expect(query_result.count).to eq(words.count)
    end
  end
end