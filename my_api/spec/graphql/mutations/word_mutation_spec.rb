RSpec.describe Mutations::WordMutation do
  describe 'creating a new record' do
    let(:args) do
      {
          name: 'RandomName'
      }
    end

    it 'increases words by 1' do
      mutation = subject.fields['create_word'].resolve(nil, args, nil)
      # adds one word to the db
      expect(mutation).to change { Word.count }.by 1
    end
  end
end