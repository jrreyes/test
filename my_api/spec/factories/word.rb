FactoryBot.define do
  factory :word do
    sequence(:name) { |n| "#{Faker::Lorem.word}-#{n}"}
  end
end