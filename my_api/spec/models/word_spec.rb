RSpec.describe Word, type: :model do
  it 'has a valid factory' do
    # Check that the factory we created is valid
    expect(build(:word)).to be_valid
  end

  let(:attributes) do
    {
        name: 'dummy_name'
    }
  end

  let(:word) { create(:word, **attributes) }

  describe 'model validations' do
    # check that the name field received the right values
    it { expect(word).to allow_value(attributes[:name]).for(:name) }
    # ensure that the name field is never empty
    it { expect(word).to validate_presence_of(:name) }
    # ensure that the name is unique for each word
    it { expect(word).to validate_uniqueness_of(:name)}
  end

end
