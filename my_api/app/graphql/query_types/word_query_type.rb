module QueryTypes
  WordQueryType = GraphQL::ObjectType.define do
    name 'WordType'
    description 'The word query type'

    field :words, types[Types::WordType], 'returns all words' do
      resolve ->(_obj, _args, _ctx) { Word.all }
    end

    field :random_word, types[Types::WordType], 'return random word' do
      resolve ->(_obj, _args, _ctx) { Word.order('RANDOM()').limit(1) }
    end
  end
end