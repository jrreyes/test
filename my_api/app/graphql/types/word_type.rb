module Types
  WordType = GraphQL::ObjectType.define do
    name 'Word'
    description 'The Word type'

    field :id, !types.ID
    field :name, !types.String

  end
end