module Mutations
  WordMutation = GraphQL::ObjectType.define do
    name 'WordMutation'
    description 'Mutation type for word'

    field :create_word, Types::WordType do
      argument :name, !types.String

      resolve ->(_obj, args, _ctx) do
        Word.create(
            name: args[:name]
        )
      end
    end
  end
end